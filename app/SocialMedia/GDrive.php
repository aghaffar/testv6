<?php

namespace App\SocialMedia;

use File;
use Storage;

class GDrive
{
    /**
     * Upload a file from local directory to a path on Google Drive. It creates directories if not already there.
     *
     * @param string  $filePath local path to file
     * @param string  $destinationURL google drive path
     * @return void
     */
    public function uploadFromLocal(string $filePath, string $destinationURL)
    {
        $dir = '/';
        // Get subdirectories also?
        $recursive = false;
        //get directories over gdrive
        $contentList = Storage::disk('google')->listContents($dir, $recursive);
        //store the directories on given path as collection
        $contents = collect($contentList);
        $filePathArray = explode("\\", $filePath);
        $filename = array_pop($filePathArray);
        $destArray = explode('/', $destinationURL);
        $fileData = File::get($filePath);

        //stores google drive directories paths
        $cart = array();

        foreach ($destArray as $valueFolder) {
            $dir = $contents->where('type', '=', 'dir')
                ->where('filename', '=', $valueFolder)
                ->first(); // There could be duplicate directory names!
            $contentOnPath = Storage::disk('google')->listContents($dir['path'], $recursive);
            $contents = collect($contentOnPath);

            if (!$dir) {

                $dest = implode('/', $cart);
                Storage::disk('google')->makeDirectory($dest . '/' . $valueFolder);
                $contentListDirec = Storage::disk('google')->listContents($dest, $recursive);
                $contents = collect($contentListDirec);
                $dir = $contents->where('type', '=', 'dir')
                    ->where('filename', '=', $valueFolder)
                    ->first(); // There could be duplicate directory names!
            }
            $cart[] = $dir['path'];
        }
        $dest = implode('/', $cart);
        Storage::disk('google')->put($dest . '/' . $filename, $fileData);

        //return 'File was created in the sub directory in Google Drive';

    }
}

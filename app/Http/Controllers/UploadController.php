<?php

namespace App\Http\Controllers;

use App\SocialMedia\GDrive;
use Illuminate\Http\Request;

class UploadController extends Controller
{

    public function index(GDrive $gdrivenew)
    {
        $filename = 'php.png';
        $filePath = public_path($filename);
        $destinationUrl = 'new/new3/new3';

        $gd = $gdrivenew->uploadFromLocal($filePath, $destinationUrl);
        return view('welcome', [
            'gdrive' => $gd,
        ]);

    }

    public function uploadFile(Request $req)
    {

        dd($req->file);
        $req->file->store("", 'google');
        return "File has been uploaded successfully.";
    }
}
